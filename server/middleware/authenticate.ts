import  { User } from '../models/user';
import * as express from "express";


let authenticate = (req: express.Request, res: express.Response, next: express.NextFunction) => {
  let token = req.header('x-auth');
  User.findByToken(token)
  .then((user) => {
    if(!user) {
      return Promise.reject();
    }
    (<any>req).user = user;
    (<any>req).token = token;
    next();
  })
  .catch((e) => {
    res.status(401).send(e);
  })
};

export { authenticate };
