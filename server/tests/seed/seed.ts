import { ObjectID } from 'mongodb';
import { User } from './../../models/user';
import { Todo } from './../../models/todo';
import * as jwt from 'jsonwebtoken';

const userOneId = new ObjectID();
const userTwoId = new ObjectID();

const users = [{
  _id: userOneId,
  password: 'UserOnePass',
  email: 'one@example.com',
  tokens: [{
    access: 'auth',
    token: jwt.sign({_id: userOneId, access: 'auth'}, process.env.JWT_SECRET).toString()
  }]
}, {
  _id: userTwoId,
  password: 'UserTwoPass',
  email: 'two@example.com',
  tokens: [{
    access: 'auth',
    token: jwt.sign({_id: userTwoId, access: 'auth'}, process.env.JWT_SECRET).toString()
  }]
}];

const todos = [{
  _id : new ObjectID(),
  text: "Second text todo",
  _creator: userOneId

}, {
  _id : new ObjectID(),
  completed : true,
  completedAt: 1538496158415.0,
  text : "First text todo",
  _creator: userTwoId
}];

const populateTodos = (done: Function) => {
  Todo.deleteMany({})
    .then(() => {
      return Todo.insertMany(todos);
    })
    .then(() => {
      done();
    });
}

const populateUsers = (done: Function) => {
  User.deleteMany({})
    .then(() => {
      let userOne = new User(users[0]).save();
      let userTwo = new User(users[1]).save();
      return Promise.all([userOne, userTwo])
    })
    .then(() => {done()});
}

export {
  todos,
  populateTodos,
  users,
  populateUsers
}
