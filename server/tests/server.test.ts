require('../config/config.ts');
import * as chai from 'chai';
import {ObjectID} from 'mongodb';
import {app} from './../server';
import {Todo} from './../models/todo';
import {User} from './../models/user';
import {todos, populateTodos, users, populateUsers} from './seed/seed';
import chaiHttp = require('chai-http');

const expect = chai.expect;
chai.use(chaiHttp);

beforeEach(populateUsers);
beforeEach(populateTodos);

describe('POST /todos', () => {
  it('should create a new todo', (done) => {
    let text = 'Test todo text';

    chai.request(app)
      .post('/todos')
      .set('x-auth', users[0].tokens[0].token)
      .send({text})
      .end((err: any, res: any) => {
        if(err) {
          return done(err);
        }
        expect(res).to.have.status(200);
        expect(res.body.text).to.be.equal(text);
        Todo.find({text})
         .then((todos: { text: any; }[]) => {
           expect(todos.length).to.be.equal(1);
           expect(todos[0].text).to.be.equal(text);
           done();
         })
         .catch(e => done(e));
      })
  });

  it('should not create a new todo with invalid data', (done) => {

    chai.request(app)
      .post('/todos')
      .set('x-auth', users[0].tokens[0].token)
      .send({})
      .end((err: any, res: any) => {
        if(err) {
          return done(err);
        }
        expect(res).to.have.status(400);
        Todo.find()
         .then((todos: { length: any; }) => {
           expect(todos).to.be.lengthOf(2);
           done();
         })
         .catch(e => done(e));
      })
  });

});

describe('GET /todos', () => {
  it('should get all todos', (done) => {
    chai.request(app)
      .get('/todos')
      .set('x-auth', users[0].tokens[0].token)
      .end((err, res) => {
        if(err) {
          return done(err);
        }
        expect(res).to.have.status(200);
        expect(res.body.todos).to.be.lengthOf(1);
        done();
      })
  });
});

describe('GET /todos/:id', () => {
  it('should get todo with proper id', (done) => {
    chai.request(app)
      .get(`/todos/${todos[0]._id.toHexString()}`)
      .set('x-auth', users[0].tokens[0].token)
      .end((err, res) => {
        if(err) {
          return done(err);
        }
        expect(res).to.have.status(200);
        expect(res.body.todo.text).to.be.equal(todos[0].text);
        done();
      })
  });

  it('should not get todo with proper id but owned by someone else', (done) => {
    chai.request(app)
      .get(`/todos/${todos[0]._id.toHexString()}`)
      .set('x-auth', users[1].tokens[0].token)
      .end((err, res) => {
        if(err) {
          return done(err);
        }
        expect(res).to.have.status(404);
        done();
      });
  });

  it('should return 404 if todo not found', (done) => {
    let hexId = new ObjectID().toHexString();
    chai.request(app)
      .get(`/todos/${hexId}`)
      .set('x-auth', users[0].tokens[0].token)
      .end((err, res) => {
        if(err) {
          return done(err);
        }
        expect(res).to.have.status(404);
        done()
      });
  });

  it('should return 404 for non object ids', (done) => {
    chai.request(app)
      .get(`/todos/123`)
      .set('x-auth', users[0].tokens[0].token)
      .end((err, res) => {
        if(err) {
          return done(err);
        }
        expect(res).to.have.status(404);
        done()
      });
  });
});

describe('DELETE /todos/:id', () => {
  it('should delete given todo', (done) => {
    let hexId = todos[1]._id.toHexString();
    chai.request(app)
      .delete(`/todos/${hexId}`)
      .set('x-auth', users[1].tokens[0].token)
      .end((err, res) => {
        if(err) {
          return done(err);
        }
        expect(res).to.have.status(200);
        expect(res.body.todo._id).to.be.equal(hexId);
        Todo.findById(hexId)
         .then((todo: any) => {
           expect(todo).to.be.null;
           done();
         })
         .catch(e => done(e));
      })
  });

  it('should not delete todo created by other user', (done) => {
    let hexId = todos[1]._id.toHexString();
    chai.request(app)
      .delete(`/todos/${hexId}`)
      .set('x-auth', users[0].tokens[0].token)
      .end((err, res) => {
        if(err) {
          return done(err);
        }
        expect(res).to.have.status(404);
        Todo.findById(hexId)
         .then((todo) => {
           expect(todo).to.be.not.empty
           done();
         })
         .catch(e => done(e));
      })
  });

  it('should return 404 if todo not found', (done) => {
    let hexId = new ObjectID().toHexString();
    chai.request(app)
      .delete(`/todos/${hexId}`)
      .set('x-auth', users[1].tokens[0].token)
      .end((err, res) => {
        if(err) {
          return done(err);
        }
        expect(res).to.have.status(404)
        done();
      });
  });

  it('should return 404 for non object ids', (done) => {
    chai.request(app)
      .delete(`/todos/123`)
      .set('x-auth', users[1].tokens[0].token)
      .end((err, res) => {
        if(err) {
          return done(err);
        }
        expect(res).to.have.status(404)
        done();
      });
    });
});

describe('PUT /todos/:id', () => {
  it('should update given todo', (done) => {
    let hexId = todos[0]._id.toHexString();
    let newBody = {
      text: "New text",
      completed: true
    };
    chai.request(app)
      .put(`/todos/${hexId}`)
      .set('x-auth', users[0].tokens[0].token)
      .send(newBody)
      .end((err, res) => {
        if(err) {
          return done(err);
        }
        expect(res).to.have.status(200);
        expect(res.body.todo.text).to.be.equal(newBody.text);
        expect(res.body.todo.completed).to.be.equal(newBody.completed);
        expect(typeof res.body.todo.completedAt).to.be.equal('number');
        done();
      });
    });

  it('should not update todo created by someone else', (done) => {
    let hexId = todos[1]._id.toHexString();
    let newBody = {
      text: "New text",
      completed: true
    };
    chai.request(app)
      .put(`/todos/${hexId}`)
      .set('x-auth', users[0].tokens[0].token)
      .send(newBody)
      .end((err, res) => {
        if(err) {
          return done(err);
        }
        expect(res).to.have.status(404);
        done();
     });
  });

  it('should clear completedAt when todo is not completed', (done) => {
    let hexId = todos[1]._id.toHexString();
    let newBody = {
      completed: false
    };
    chai.request(app)
      .put(`/todos/${hexId}`)
      .set('x-auth', users[1].tokens[0].token)
      .send(newBody)
      .end((err, res) => {
        if(err) {
          return done(err);
        }
        expect(res).to.have.status(200);
        expect(res.body.todo.completed).to.be.equal(newBody.completed);
        expect(res.body.todo.completedAt).to.be.null;
        done();
     });
  });
});

describe('GET /users/me', () => {
  it('should return authenticated user', (done) => {
    chai.request(app)
      .get(`/users/me`)
      .set('x-auth', users[0].tokens[0].token)
      .end((err, res) => {
        if(err) {
          return done(err);
        }
        expect(res.body._id).to.be.equal(users[0]._id.toHexString());
        expect(res.body.email).to.be.equal(users[0].email);
        done();
     });
  });

  it('should return 401', (done) => {
    chai.request(app)
      .get(`/users/me`)
      .end((err, res) => {
        if(err) {
          return done(err);
        }
        expect(res).to.have.status(401);
        done();
      });
  });
});

describe('POST /users', () => {
  it('should create a user', (done) => {
    const email = 'testemail@example.com';
    const password = 'testowe';
    chai.request(app)
      .post('/users')
      .send({email, password})
      .end((err, res) => {
        if (err) return done(err);
        expect(res).to.have.status(200);
        expect(res.header['x-auth']).to.be.not.null;
        expect(res.body._id).to.be.not.null;
        expect(res.body.email).to.be.equal(email);
        User.findOne({ email }).then((user: { email: any; password: any; }) => {
            expect(user.email).to.be.equal(email);
            expect(user.password).not.to.be.equal(password);
            done();
          })
          .catch(e => done(e));
      })

  });

  it('should return validation errors if request invalid', (done) => {
    let email = 'testemail@example';
    let password = 't';
    chai.request(app)
      .post('/users')
      .send({email, password})
      .end((err, res) => {
        if(err) return done(err);
        expect(res).to.have.status(400);
        done();
      });
  });

  it('should not create a user if email in use', (done) => {
    let password = 'testowe';
    chai.request(app)
      .post('/users')
      .send({email: users[0].email, password})
      .end((err, res) => {
        if(err) return done(err);
        expect(res).to.have.status(400);
        done();
      });
  });
});

describe('POST /users/login', () => {
  it('should return 400 when invalid credentials', (done) => {
  let email = users[1].email;
  let password = users[1].password+'aaa';
  chai.request(app)
    .post('/users/login')
    .send({
      email,
      password
    })
    .end((err, res) => {
      if (err) return done(err);
      expect(res).to.have.status(400);
      expect(res.header['x-auth']).to.be.undefined;
      User.findById(users[1]._id)
        .then((user: { tokens: { length: any; }; }) => {
          expect(user.tokens).to.be.lengthOf(1);
          done();
        })
        .catch(e => done(e));
    })
  })

  it('login a user', (done) => {
    let email = users[1].email;
    let password = users[1].password;
    chai.request(app)
      .post('/users/login')
      .send({
        email,
        password
      })
      .end((err, res) => {
        if (err) return done(err);
        expect(res).to.have.status(200);
        expect(res.header['x-auth']).to.be.not.false;
        User.findById(users[1]._id)
          .then((user: { toObject: () => { tokens: any[]; }; }) => {
            expect(user.toObject().tokens[1]).to.include({
              access: 'auth',
              token: res.header['x-auth']
            });
            done();
          })
          .catch(e => done(e));
      })
  })
});

describe('DELETE /users/me/logout', () => {
  it('should remove token of user when logging out', (done) => {
    chai.request(app)
      .delete('/users/me/token')
      .set('x-auth', users[0].tokens[0].token)
      .end((err, res) => {
        if (err) return done(err);
        expect(res).to.have.status(200);
        expect(res.header['x-auth']).to.be.undefined;
        User.findById(users[0]._id)
          .then((user: { tokens: { length: any; }; }) => {
            expect(user.tokens.length).to.be.equal(0);
            done();
          })
          .catch(e => done(e));
      })
  });

  it('should not logout user when logged out', (done) => {
    chai.request(app)
      .delete('/users/me/token')
      .end((err, res) => {
        if (err) return done(err);
        expect(res).to.have.status(401);
        User.findById(users[0]._id)
          .then((user: { tokens: { length: any; }; }) => {
            expect(user.tokens).to.be.lengthOf(1);
            done();
          })
          .catch(e => done(e));
      })
  });
});
