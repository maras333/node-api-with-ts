require('./config/config.ts');
import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as _ from 'lodash';

import { ObjectID } from 'mongodb';
import { Todo } from './models/todo';
import { User } from './models/user';
import { authenticate } from './middleware/authenticate';
const app = express();
const port = process.env.PORT;
app.use(bodyParser.json());

app.post('/todos', authenticate, async (req: express.Request, res: express.Response) => {
  let todo = new Todo({
    text: req.body.text,
    _creator: (<any>req).user._id
  })
  try {
    let doc = await todo.save();
    res.send(doc)
  } catch (e) {
    res.status(400).send(e);
  }
});

app.get('/todos', authenticate, async (req: express.Request, res: express.Response) => {
  try {
    let todos = await Todo.find({_creator: (<any>req).user._id});
    res.send({todos})
  } catch (e) {
    res.status(400).send();
  }
});

app.get('/todos/:id', authenticate, async (req: express.Request, res: express.Response) => {
  if(!ObjectID.isValid(req.params.id)) {
    res.status(404).send();
  }
  try {
    let todo = await Todo.findOne({
      _id: req.params.id,
      _creator: (<any>req).user.id
    });
    if(todo) {
      res.send({todo});
    } else {
      res.status(404).send();
    }
  } catch (e) {
    res.status(400).send();
  }
});

app.delete('/todos/:id', authenticate, async (req: express.Request, res: express.Response) => {
  if(!ObjectID.isValid(req.params.id)) {
    res.status(404).send();
  }
  try {
    let todo = await Todo.findOneAndDelete({
      _id: req.params.id,
      _creator: (<any>req).user.id
    })
    if(todo) {
      res.send({todo});
    } else {
      res.status(404).send();
    }
  } catch (e) {
    res.status(400).send();
  }
});

app.put('/todos/:id', authenticate, async (req: express.Request, res: express.Response) => {
  let id = req.params.id;
  let body: object = _.pick(req.body, ['text', 'completed']);

  if(!ObjectID.isValid(id)) {
    res.status(404).send();
  }

  if(_.isBoolean((<any>body).completed) && (<any>body).completed) {
    (<any>body).completedAt = new Date().getTime();
  } else {
    (<any>body).completed = false;
    (<any>body).completedAt = null;
  }

  try {
    let todo = await Todo.findOneAndUpdate({
      _id: req.params.id,
      _creator: (<any>req).user._id
    }, {
      $set: body
    }, {
      new: true
    })
    if(!todo) {
      return res.status(404).send();
    }
    res.send({todo});
  } catch (e) {
    res.status(400).send();
  }
});

app.get('/users/me', authenticate, (req: express.Request, res: express.Response) => {
  res.send((<any>req).user);
});

app.post('/users/login', async (req: express.Request, res: express.Response) => {
  let body = _.pick(req.body, ['email', 'password']);
  try {
    let user = await User.findByCredentials(body);
    let token = await user.generateAuthToken();
    res.header('x-auth', token).send({user});
  } catch (e) {
    res.status(400).send()
  }
});

app.delete('/users/me/token', authenticate, async (req: express.Request, res: express.Response) => {
  try {
    await (<any>req).user.removeToken((<any>req).token);
    res.status(200).send();
  } catch (e) {
    res.status(400).send();
  }
});



app.post('/users', async (req: express.Request, res: express.Response) => {
  try {
    let body = _.pick(req.body, ['email', 'password']);
    let user = new User(body);
    await user.save();
    let token = await user.generateAuthToken();
    res.header('x-auth', token).send(user)
  } catch (e) {
    res.status(400).send({ msg: e });
  }
});

app.listen(port, () => {
  console.log(`Started on port ${port}`);
});

export { app };
