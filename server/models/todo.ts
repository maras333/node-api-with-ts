import { Document, Model, Schema } from "mongoose";
import { connection } from '../db/mongoose';

export interface ITodoDocument extends Document {
  text: String,
  completed: Boolean,
  completedAt: Number,
  _creator: Schema.Types.ObjectId
}

export interface ITodoModel extends Model<ITodoDocument> {
  // TODO: add static methods here
}

const TodoSchema: Schema = new Schema({
  text: {
    type: String,
    required: true,
    minlength: 1,
    trim: true
  },
  completed: {
    type: Boolean,
    default: false
  },
  completedAt: {
    type: Number,
    default: null
  },
  _creator: {
    type: Schema.Types.ObjectId,
    required: true
  }
});

export const Todo: ITodoModel = connection.model<ITodoDocument, ITodoModel>('Todo', TodoSchema);
