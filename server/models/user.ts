import { Document, Model, Schema } from "mongoose";
import { connection } from '../db/mongoose';
import * as validator from 'validator';
import * as jwt from 'jsonwebtoken';
import * as _ from'lodash';
import * as bcrypt from 'bcryptjs';
import * as express from "express";


export interface IUserDocument extends Document {
  email: string,
  password: string,
  tokens: [{
    access: string,
    token: string
  }],
  generateAuthToken(): string
}

export interface IUserModel extends Model<IUserDocument> {
  findByToken(token: string): Promise<IUserDocument>,
  findByCredentials(body: Object): Promise<IUserDocument>
}

const UserSchema = new Schema({
  email: {
    type: String,
    required: true,
    trim: true,
    minlength: 1,
    unique: true,
    validate: {
      validator: validator.isEmail,
      message: '{VALUE} is not an email'
    }
  },
  password: {
    type: String,
    required: true,
    minlength: 6
  },
  tokens: [{
    access: {
      type: String,
      required: true
    },
    token: {
      type: String,
      required: true
    }
  }]
});

UserSchema.static('findByToken', function (token: string) {
  let User = this;
  let decoded: object | string;
  try {
    decoded = jwt.verify(token, process.env.JWT_SECRET);
  } catch (e) {
    return Promise.reject();
  }
  return User.findOne({
    '_id': (<any>decoded)._id,
    'tokens.token': token,
    'tokens.access': 'auth'
  });
});

UserSchema.static('findByCredentials', async function (body: object) {
  let User = this;
  let email = (<any>body).email;
  let password = (<any>body).password;

  try {
    let user = await User.findOne({email});
    if(!user) {
      return Promise.reject('User not found');
    }
    return new Promise((resolve, reject) => {
      bcrypt.compare(password, user.password, (err, res) => {
        if(res) {
          resolve(user);
        } else {
          reject(err);
        }
      })
    })
  } catch (e) {
    return Promise.reject('Internal error');
  }
});

UserSchema.method('toJSON', function () {
  let user = this;
  return _.pick(user, ['_id', 'email']);
});


UserSchema.method('generateAuthToken', async function () {
  let user = this;
  let access = 'auth';
  let token = jwt.sign({
    _id: user._id.toHexString(),
    access
  }, process.env.JWT_SECRET).toString();

  user.tokens = user.tokens.concat([{
    access,
    token
  }]);

  user.save();
  return token;

});

UserSchema.method('removeToken', function (token: string) {
  let user = this;
  return user.updateOne({
    $pull: {
      tokens: { token }
    }
  });

});

UserSchema.pre('save', async function(next: express.NextFunction) {
  let user = this;
  if (user.isModified('password')) {
    try {
      let salt = await bcrypt.genSalt(10);
      let hash = await bcrypt.hash((<any>user).password, salt);
      (<any>user).password = hash;
      next();
    } catch (e) {
      console.log(e);
    }
  } else {
    next();
  }
});

export const User: IUserModel = connection.model<IUserDocument, IUserModel>('User', UserSchema);
