import * as mongoose from 'mongoose';

(mongoose as any).Promise = global.Promise;
mongoose.connect(process.env.MONGODB_URI,  { useNewUrlParser: true });
mongoose.set('useCreateIndex', true);
const connection = mongoose;

export { connection }
