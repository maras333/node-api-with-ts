const {
  MongoClient,
  ObjectID
} = require('mongodb');

MongoClient.connect('mongodb://localhost:27017/TodoApp', {
  useNewUrlParser: true
}, (err, client) => {
  if (err) {
    return console.log('Unable to connect to MongoDB server');
  }
  console.log('Connected to MongoDB server');

  const db = client.db('TodoApp');

  // deleteMany
  db.collection('Todos').deleteMany({
      text: 'Laundry'
    })
    .then((data) => {
      console.log(`Removed ${data.result.n} items.`);
    })
    .catch(e => {
      console.log('Unable to count todos', e);
    })

  // deleteOne
  db.collection('Todos').deleteOne({
      text: 'Laundry'
    })
    .then((data) => {
      console.log(`Removed ${data.result.n} items.`);
    })
    .catch(e => {
      console.log('Unable to count todos', e);
    })

  // findOneAndDelete
  db.collection('Todos').findOneAndDelete({
      text: 'Laundry'
    })
    .then((data) => {
      console.log(data);
    })
    .catch(e => {
      console.log('Unable to count todos', e);
    })
  //


  // client.close();
});